<p align="center"></p><hr><h1 align="center">Hello World<img src="https://raw.githubusercontent.com/ABSphreak/ABSphreak/master/gifs/Hi.gif" width="30px">, I'm Betrand Mutangana Ishimwe</h1><h3 align="center">Stand-alone Front-end Software Developer  </h3><h4 align="center">I'm 2 years old in programming experience.</h4>
<p align="center">I have much interest in React, Three, and Redux. I'm also good at content creation, website designing, and some sort of 3D animation. <br>
  I'm constantly striving to build systems and tools that improve the community's way of living. Curiosity is the one that pushes me to learn more and seek for more adventure in this internet world. Do you know what is trending? <strong>Web3</strong> That is why I'm learning new technologies to dive into web3.</p>

<p align="center">
  <em>I'm always <b>
fascinated</b>
    about new challenges to 
    <b>grow</b> <img src="https://github.com/TheDudeThatCode/TheDudeThatCode/blob/master/Assets/Rocket.gif" width="18px">and 
    <b>excel</b> <img src="https://github.com/TheDudeThatCode/TheDudeThatCode/blob/master/Assets/Medal.gif" width="20px">&nbsp
    because as you know <b>What doesn't kill you, makes you stronger.</b>
  </em> 
  <br>
  
</p>
<hr>
<h2><u><b>Knowledge Base</b></u></h2>

<h3>Languages</h3>
<p>
   <a href="https://www.php.net/" target="_blank"> 
    <img src="https://img.shields.io/badge/PHP-777BB4?style=for-the-badge&logo=php&logoColor=white" 
      alt="php"/> 
  </a>

  <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> 
    <img src="https://img.shields.io/badge/Javascript-F7DF1E.svg?style=for-the-badge&logo=javascript&logoColor=black"
      alt="javascript"/> 
  </a>
  <a href="https://www.w3.org/html/" target="_blank"> 
    <img src="https://img.shields.io/badge/html-E34F26.svg?style=for-the-badge&logo=html5&logoColor=white"
      alt="html5"/> 
  </a>
  <a href="https://www.w3schools.com/css/" target="_blank">
    <img src="https://img.shields.io/badge/css-1572B6.svg?style=for-the-badge&logo=css3&logoColor=white"
      alt="css3"/>
  </a>
   <a href="https://www.json.org/json-en.html" target="_blank">
    <img src="https://img.shields.io/badge/json-5E5C5C?style=for-the-badge&logo=json&logoColor=white"
      alt="json"/>
  </a>
</p>
<p>
<h3>Frontend</h3>
<p>
      <a href="https://getbootstrap.com" target="_blank">
    <img src="https://img.shields.io/badge/bootstrap-7952B3.svg?style=for-the-badge&logo=bootstrap&logoColor=white"
      alt="bootstrap"/>
  </a>
  <a href="https://angular.io/" target="_blank">
    <img src="https://angular.io/assets/images/logos/angular/logo-nav@2x.png" alt="angular" width="70px"/> 
  </a>

  <a href="https://reactjs.org/" target="_blank"> 
    <img src="https://img.shields.io/badge/reactjs-61DAFB.svg?style=for-the-badge&logo=react&logoColor=black"
      alt="react"/> 
  </a>
  
  ![React Native](https://img.shields.io/badge/-React%20Native-000000?style=flat&logo=react&labelColor=000000)
  
  <a href="https://redux.js.org" target="_blank"> 
    <img src="https://img.shields.io/badge/redux-764ABC.svg?style=for-the-badge&logo=redux&logoColor=white" alt="redux"/> 
  </a> 
  <a href="https://jquery.com/" target="_blank">
    <img src="https://img.shields.io/badge/jquery-0769AD.svg?style=for-the-badge&logo=jquery&logoColor=white" alt="jquery"/> 
  </a>
  <a href="https://webpack.js.org" target="_blank">
    <img src="https://img.shields.io/badge/webpack-8DD6F9.svg?style=for-the-badge&logo=webpack&logoColor=black"
      alt="webpack"/>
  </a>
    <a href="https://https://threejs.org/" target="_blank">
    <img src="https://img.shields.io/badge/Three-three--js-blue"
      alt="Threejs"/>
  </a>
      <a href="https://www.typescriptlang.org/" target="_blank">
    <img src="https://img.shields.io/badge/Typescript-Typescript-yellow"
      alt="Typescript"/>
  </a>
  
 ![NPM](https://img.shields.io/badge/-npm-000000?style=flat&logo=npm&labelColor=ffffff)
  
 ![Sass](https://img.shields.io/badge/-Sass-000000?style=flat&logo=sass&logoColor=ffffff&labelColor=%23CC6699)
  
![Font Awesome](https://img.shields.io/badge/-font%20awesome-000000?style=flat&logo=font-awesome&logoColor=339AF0&labelColor=ffffff)
  
  
</p>
<p>
<h3>Backend</h3>
<p>

   <a href="https://www.mysql.com/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="40" height="40"/> </a>
</p> 
<h3>Database</h3>
<p>
  <a href="https://www.mysql.com/" target="_blank"> 
    <img src="https://img.shields.io/badge/MySQL-005C84?style=for-the-badge&logo=mysql&logoColor=white"
      alt="mysql"/>
  </a>
 
</p>
<p>
<h3>Cloud & Hosting:</h3>
<p>
  <a href="https://cloud.google.com/" target="_blank">
    <img  src="https://img.shields.io/badge/Google_Cloud-4285F4?style=for-the-badge&logo=google-cloud&logoColor=white" alt="Google cloud"/> 
  </a>
  <a href="https://firebase.google.com/" target="_blank">
    <img src="https://img.shields.io/badge/firebase-FFCA28.svg?style=for-the-badge&logo=firebase&logoColor=black" alt="firebase"/>
  </a>
  <a href="https://netlify.com/" target="_blank">
    <img src="https://img.shields.io/badge/netlify-00C7B7.svg?style=for-the-badge&logo=netlify&logoColor=black" alt="firebase"/>
  </a>
  <a href="https://heroku.com" target="_blank"> 
    <img src="https://img.shields.io/badge/heroku-430098.svg?style=for-the-badge&logo=heroku&logoColor=white"
      alt="heroku"/> 
  </a> 
</p>
<p>
<h3>Version Control & CI/CD</h3>
<p>
  <a href="https://git-scm.com/" target="_blank">
    <img src="https://img.shields.io/badge/git-F05032.svg?style=for-the-badge&logo=git&logoColor=white"
      alt="git"/>
  </a>
  <a href="https://github.com/ELanza-48" target="_blank">
    <img src="https://img.shields.io/badge/github-181717.svg?style=for-the-badge&logo=github&logoColor=white" alt="github" />
  </a>
</p>
<p>
<h3>Preferred IDEs & Tools :</h3>
<p> 
   <a href="https://developer.android.com/" target="_blank">
    <img src="https://img.shields.io/badge/Android_Studio-3DDC84?style=for-the-badge&logo=android-studio&logoColor=white" alt="Android Studio"/> 
  </a>
  <a href="https://code.visualstudio.com/" target="_blank">
    <img src="https://img.shields.io/badge/vscode-007ACC.svg?style=for-the-badge&logo=visualstudiocode&logoColor=white" alt="vsCode"/> 
  </a>
  <a href="https://postman.com" target="_blank"> 
    <img src="https://img.shields.io/badge/postman-FF6C37.svg?style=for-the-badge&logo=postman&logoColor=white" alt="postman"/>
  </a>
  <a href="https://www.microsoft.com/fr-fr/windows" target="_blank"> 
    <img src="https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white" alt="windows"/>
  </a>
</p>

----

<h3>Connect with me</h3>

<div style="margin-top:10px">
  <div>
    <a  href="https://www.linkedin.com/in/bertrand-mutangana-ishimwe-024905220/" target="_blank">
      <img src="https://img.shields.io/badge/Linked%20In-0A66C2.svg?style=for-the-badge&logo=linkedin&logoColor=white" alt="lindedIn"/>
    </a>
    <a href="https://twitter.com/BertrandMutanga" target="_blank">
      <img src="https://img.shields.io/badge/Twitter-1DA1F2.svg?style=for-the-badge&logo=twitter&logoColor=white" alt="twitter"/>
    </a>
  </div>
</div>
<p>
<h3>Reach me</h3>

<p>
  <a href="mailto:mutanganabertrand@gmail.com?subject=Feedback%20From%20Github&body=Hello," target="_blank">
    <img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" alt="email"/>
  </a>
</p>
  

<br><br>

# Github's Status

  <p align="center">
   <a href="https://github.com/shoirata">
    <img height="180em" src="https://github-readme-stats.vercel.app/api?username=BertrandConxy&show_icons=true&theme=radical"/>
    <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=BertrandConxy&show_icons=true&theme=midnight-white&layout=compact" alt="shoira" />
  </a>
</p>
</h3>



Please feel free to clone/fork projects, raise issues and submit PRs if you think something could be better.
Ask me anything here
or email me 
http://mutanganabertrand@gmail.com

⭐️ From [Bertrand Mutangana](https://github.com/BertrandConxy)

## Happy Coding! 😊

<!--
**BertrandConxy/BertrandConxy** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.



